﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class BidirectionalPointGraph
{

    public HashSet<GraphNode> nodes;

    Dictionary<GraphNode, Dictionary<GraphNode, GraphEdge>> edges;
    public Dictionary<NodeType, List<GraphNode>> typeToNodes;

    public BidirectionalPointGraph()
    {
        typeToNodes = new Dictionary<NodeType, List<GraphNode>>();
        edges = new Dictionary<GraphNode, Dictionary<GraphNode, GraphEdge>>();
        nodes = new HashSet<GraphNode>();
    }

    public void AddEdge(GraphNode from, GraphNode to)
    {
        from.OnDestroy += From_OnDestroy;
        if (!edges.ContainsKey(from))
            edges.Add(from, new Dictionary<GraphNode, GraphEdge>());

        if (!edges[from].ContainsKey(to))
            edges[from].Add(to, from.ConnectTo(to));
        else
            throw new GraphException("Trying to add double edge.");
    }

    private void From_OnDestroy(GraphNode obj)
    {
        if (edges.ContainsKey(obj))
        {
            edges.Remove(obj);            
        }
        foreach (GraphNode gn in obj.connectedNodes)
        {
            if (edges.ContainsKey(gn))
            {
                edges.Remove(gn);
            }
        }
        if (nodes.Contains(obj))
            nodes.Remove(obj);
    }


    public void AddNode(NodeType type, GraphNode node)
    {
        if (!typeToNodes.ContainsKey(type))
            typeToNodes.Add(type, new List<GraphNode>());

        typeToNodes[type].Add(node);
        nodes.Add(node);
    }

    public GraphEdge GetEdge(GraphNode from, GraphNode to)
    {
        if (!edges.ContainsKey(from))
            return null;

        if (!edges[from].ContainsKey(to))
            return null;

        return edges[from][to];
    }
}

public class GraphException : Exception
{
    public GraphException(string message) : base(message) { }
}

