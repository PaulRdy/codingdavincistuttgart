﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using System;
using UnityEngine.UI;
using UnityEngine.Events;
public class UI : MonoBehaviour
{
    public static UI Instance
    {
        get
        {
            return _instance;
        }
    }
    static UI _instance;

    public TMP_Text LoadingDetailText { get { return loadingDetailText; } }
    [SerializeField] TMP_Text loadingDetailText;
    [SerializeField] UIScreen loadingScreen;
    [SerializeField] UIScreen ingameScreen;
    [SerializeField] TMP_Text text;
    [SerializeField] GameObject selectionPreview;
    [SerializeField] GameObject actionConfirmPanel;
    [SerializeField] TMP_Text actionText;
    [SerializeField] Button actionConfirmButton, actionDeclineButton;
    [SerializeField] GameObject percentFilledPanel;
    [SerializeField] Image percentFilledImage;
    [SerializeField] GameObject spectatingScreen;
    [SerializeField] Image overlayFadeToBlackImage;
    Dictionary<string, UIScreen> uiScreens;
    Animator animator;

    void Awake()
    {
        if (_instance == null)
        {
            Init();
        }
    }
    private void Update()
    {
        text.text = ""/*(1.0f / Time.deltaTime).ToString()*/;
    }
    public static void __INIT()
    {
        if (_instance == null)
        {
            FindObjectOfType<UI>().Init();
        }
    }
    private void Init()
    {
        _instance = this;
        uiScreens = new Dictionary<string, UIScreen>();
        animator = GetComponent<Animator>();

        foreach (Transform t in transform)
        {
            t.gameObject.SetActive(true);
        }
        foreach(UIScreen uiComp in GetComponentsInChildren<UIScreen>())
        {
            uiComp.Init();
            uiScreens.Add(uiComp.name, uiComp);
            Debug.Log(uiComp.name);
        }
        SubscribeMessages();
    }

    private void SubscribeMessages()
    {
        MessageHub.StartListening(MessageType.GameLoadingStarted, GameLoadingStarted, MethodPriority.UpdateUI);
        MessageHub.StartListening(MessageType.GameLoadingFinished, GameLoadingFinished, MethodPriority.UpdateUI);
        MessageHub.StartListening(MessageType.NodeSelected, NodeSelected, MethodPriority.UpdateUI);
        MessageHub.StartListening(MessageType.NodeConnectionProposed, ConnectionProposed, MethodPriority.UpdateUI);
        MessageHub.StartListening(MessageType.GameplayActionComissioned, ActionComissioned, MethodPriority.UpdateUI);
        MessageHub.StartListening(MessageType.BombAlarmSounded, BombAlarmSounded, MethodPriority.UpdateUI);
        MessageHub.StartListening(MessageType.BombingStarted, OnBombingStarted, MethodPriority.UpdateUI);
        MessageHub.StartListening(MessageType.BombingExecuted, OnBombingExecuted, MethodPriority.UpdateUI);
        MessageHub.StartListening(MessageType.BombingOver, OnBombingOver, MethodPriority.UpdateUI);
        MessageHub.StartListening(MessageType.TurnStarted, OnTurnStarted, MethodPriority.UpdateUI);
    }

    private void OnTurnStarted(Message obj)
    {
        UpdateActionBar();
    }

    private void OnBombingOver(Message obj)
    {
        ShowDefaultIngameScreen();
    }

    private void OnBombingExecuted(Message obj)
    {
        animator.Play("UIFadeFromBlack");
    }

    private void OnBombingStarted(Message obj)
    {
        animator.Play("UIFadeToBlack");
    }

    private void BombAlarmSounded(Message obj)
    {
        ShowSpectatingScreen();
    }

    private void ActionComissioned(Message obj)
    {
        UpdateActionBar();
    }

    private void UpdateActionBar()
    {
        float fillPercent = (float)GameManager.instance.NumberOfCurrentActions / (float)GameManager.instance.MaxNumberOfActions;
        percentFilledImage.fillAmount = fillPercent;
    }

    private void ConnectionProposed(Message obj)
    {
        NodeController from = (NodeController)obj.data[0];
        NodeController to = (NodeController)obj.data[1];
        string text = "Willst Du einen Wegweiser von " + from.gameObject.name + " nach " + to.gameObject.name + " aufstellen?";
        ShowActionPanel(text, AcceptConnection, DeclineConnection);
    }

    private void ShowActionPanel(string text, UnityAction acceptConnection, UnityAction declineConnection)
    {
        actionConfirmPanel.SetActive(true);
        actionText.text = text;
        actionConfirmButton.onClick.AddListener(acceptConnection);
        actionDeclineButton.onClick.AddListener(declineConnection);
    }

    void HideActionPanel()
    {
        actionText.text = "";
        actionConfirmButton.onClick.RemoveAllListeners();
        actionDeclineButton.onClick.RemoveAllListeners();
        actionConfirmPanel.SetActive(false);
    }

    private void DeclineConnection()
    {
        HideActionPanel();
        MessageHub.PushQueue(new Message(MessageType.UINodeConnectionDeclined, null));
    }
    

    private void AcceptConnection()
    {
        HideActionPanel();
        MessageHub.PushQueue(new Message(MessageType.UINodeConnectionAccepted, null));
    }

    private void NodeSelected(Message obj)
    {
        if(GameManager.instance.selectedNode != null)
        {
            selectionPreview.SetActive(true);
        }
        else
        {
            selectionPreview.SetActive(false);
        }
    }

    private void GameLoadingFinished(Message obj)
    {
        ShowDefaultIngameScreen();
    }

    private void GameLoadingStarted(Message obj)
    {
        ShowLoadingScreen();
    }

    public void ShowLoadingScreen()
    {
        HideAllScreens();
        loadingScreen.gameObject.SetActive(true);
    }
    public void ShowDefaultIngameScreen()
    {
        HideAllScreens();
        actionConfirmPanel.SetActive(false);
        ingameScreen.gameObject.SetActive(true);
    }
    public void ShowSpectatingScreen()
    {
        HideAllScreens();
        spectatingScreen.SetActive(true);
    }
    private void HideAllScreens()
    {
        foreach(UIScreen s in uiScreens.Values)
        {
            s.gameObject.SetActive(false);
        }
    }
}
