﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CameraController : MonoBehaviour
{

    [SerializeField] float speed = 7.0f;
    [SerializeField] Transform centerOfTown;
    [SerializeField] float moveToCenterTime = 10.0f;
    [SerializeField] BoxCollider cameraBounds;
    [SerializeField] Camera planeCamera, mainCamera;
    Animator anim;
    // Start is called before the first frame update
    private void Awake()
    {
        MessageHub.StartListening(MessageType.BombAlarmSounded, OnBombAlarm);
        MessageHub.StartListening(MessageType.BombingOver, OnBombingOver);
        anim = GetComponent<Animator>();
        if (cameraBounds == null)
            throw new System.Exception("Camera bounds not assigned. Assign in CameraController");
    }

    private void OnBombingOver(Message obj)
    {
        anim.Play("Idle");
    }

    private void OnBombAlarm(Message obj)
    {
        anim.Play("ZoomOut");
        StartCoroutine(MoveToCenterOfTownRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.state == GameState.Planning)
            this.transform.Translate(new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * speed, 0, Input.GetAxis("Vertical") * Time.deltaTime * speed));

        if (!cameraBounds.bounds.Contains(transform.position))
        {
            transform.position = cameraBounds.bounds.ClosestPoint(transform.position);
        }
        planeCamera.orthographicSize = mainCamera.orthographicSize;
    }

    IEnumerator MoveToCenterOfTownRoutine()
    {
        if (centerOfTown != null)
        {
            Vector3 trg = centerOfTown.transform.position;
            trg.y = transform.position.y;
            Vector3 dir = Vector3.Lerp(transform.position, trg, Time.deltaTime / moveToCenterTime) - transform.position;
            while (Vector3.Distance(trg, transform.position) > 0.05f)
            {
                Vector3 newPos = transform.position + dir;
                newPos.y = transform.position.y;
                transform.position = newPos;
                yield return null;
            }
        }
        else
        {
            Debug.LogWarning("Center of town was null. Camera has nowhere to move! Set the center of town in the CameraController");
        }
    }
}
