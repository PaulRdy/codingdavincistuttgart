﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskTesting : MonoBehaviour
{
    [SerializeField] Task testTask;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(r, out hit, LayerMask.GetMask("Floor")))
            {
                TaskManager.Instance.CreateTask(testTask, hit.point, Foo);
            }
        }
    }

    void Foo(TaskInstance task)
    {

    }
}
