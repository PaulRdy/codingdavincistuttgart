﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TaskManager : MonoBehaviour
{
    Dictionary<TaskInstance, List<EntityController>> tasks;

    public static TaskManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<TaskManager>();
            }
            return instance;
        }
    }
    static TaskManager instance;

    private void Awake()
    {
        tasks = new Dictionary<TaskInstance, List<EntityController>>();
    }

    private void TestRubble(TaskInstance obj)
    {
        GameManager.instance.SpawnRubble(obj.transform.position);
    }

    public TaskInstance CreateTask(Task taskToCreate, Vector3 position, Action<TaskInstance> onFinished)
    {
        TaskInstance instance = 
            GameObject.Instantiate(
                taskToCreate.Prefab, 
                position, 
                Quaternion.identity).GetComponent<TaskInstance>();
        instance.Init(taskToCreate, onFinished);
        tasks.Add(instance, new List<EntityController>());

        IEnumerable<EntityController> selectedEntitiesForTask =
            GameManager.instance.entities.
            Where(e => e.state == AIState.Idle || e.state == AIState.InBuilding).
            OrderBy(e => Vector3.Distance(e.transform.position, position)).
            Take(taskToCreate.NumberOfRequiredEntities);
        foreach (EntityController ec in selectedEntitiesForTask)
        {
            ec.NotifyWithTask(instance);
        }

        return instance;
    }
}
