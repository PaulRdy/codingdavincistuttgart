﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Task", menuName = "Entity Task", order = 1)]
public class Task : ScriptableObject
{
    public int NumberOfRequiredEntities { get { return numberOfRequiredEntities; } }
    [SerializeField] int numberOfRequiredEntities;

    public float CompleteTime { get { return completeTime; } }
    [SerializeField] float completeTime;

    public GameObject Prefab { get { return prefab; } }
    [SerializeField] GameObject prefab;

    public float MinDistanceForProcessing { get { return minDistanceForProcessing; } }
    [SerializeField] float minDistanceForProcessing;
}
