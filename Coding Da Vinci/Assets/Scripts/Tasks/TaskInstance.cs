﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskInstance : MonoBehaviour
{
    public event Action Completed;

    public Task data
    {
        get; private set;
    }
    public Collider TaskTrigger
    {
        get;
        private set;
    }
    /// <summary>
    /// progress is measured by the number of workers / numer of required workers.
    /// If the maximum numer of required workers is reached, 1 progress is made per second
    /// </summary>
    float progress;
    bool finished;
    HashSet<EntityController> workers;
    [SerializeField] Image loadingImage;
    [SerializeField] GameObject loadingCanvas;

    Action<TaskInstance> finishedAction;

    void Update()
    {
        if (finished) return;
        if (data.NumberOfRequiredEntities > 0)
        {
            progress += (workers.Count / (float)data.NumberOfRequiredEntities) * Time.deltaTime;
        }
        else
        {
            progress += Time.deltaTime;
        }
        loadingImage.fillAmount = progress / data.CompleteTime;
        if (progress >= data.CompleteTime)
        {
            FinishTask();
        }
    }

    private void FinishTask()
    {
        ReleaseWorkers();
        finished = true;
        loadingCanvas.SetActive(false);
        if (finishedAction != null)
            finishedAction(this);
        MessageHub.StopListening(MessageType.EntityReachedCollider, OnEntityReachedPosition);
        Destroy(this.gameObject);
    }

    void ReleaseWorkers()
    {
        foreach(EntityController e in workers)
        {
            e.GoHome();
        }
    }

    internal void Init(Task taskToCreate, Action<TaskInstance> onFinished)
    {
        TaskTrigger = this.gameObject.AddComponent<SphereCollider>();
        TaskTrigger.isTrigger = true;
        ((SphereCollider)TaskTrigger).radius = taskToCreate.MinDistanceForProcessing;
        data = taskToCreate;
        finishedAction = onFinished;
        loadingImage.fillAmount = 0f;
        finished = false;
        workers = new HashSet<EntityController>();
        MessageHub.StartListening(MessageType.EntityReachedCollider, OnEntityReachedPosition);
    }

    private void OnEntityReachedPosition(Message obj)
    {
        Collider col = (Collider)obj.data[1];
        EntityController entity = (EntityController)obj.data[0];

        if (col == TaskTrigger &&
            workers.Count < data.NumberOfRequiredEntities)
        {
            workers.Add(entity);
            entity.TransitionState(AIState.ExecutingTask);
        }
    }
}
