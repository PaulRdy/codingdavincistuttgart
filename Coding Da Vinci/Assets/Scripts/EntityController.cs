﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum AIState
{
    InBuilding,
    Idle,
    /// <summary>
    /// Moves via the road network using the signs
    /// </summary>
    MovingToNode,
    /// <summary>
    /// Moves via the nav mesh using the shortest path
    /// </summary>
    MovingToPosition,
    /// <summary>
    /// Moves via the navmesh to the position of a trigger object.
    /// Considers the destination reached when the entity collides with the trigger.
    /// </summary>
    MovingToTrigger,
    ExecutingTask
}

[RequireComponent(typeof(NavMeshAgent))]
public class EntityController : MonoBehaviour
{
    [SerializeField] float minNodeDist = .3f;
    [SerializeField] float minPositionDist = .3f;
    [SerializeField] NavMeshAgent navMeshAgent;

    Action nodeReachedAction;

    GraphNode destinationNode;
    GraphNode currNode;
    GraphNode nextNode;
    GraphNode homeNode;
    /// <summary>
    /// Only relevant if looking for a concrete destination node.
    /// If a node type is being looked for the  nodes themselves say which next node the target should be.
    /// </summary>
    List<AstarNode> currPathToDestinationNode;
    int currentPathIndex;
    IBuilding occupiedBuilding;
    NodeType currentDesiredNodeType;
    EntityBank bank;
    Vector3 currentTargetPosition;
    Collider currentTargetCollider;

    public AIState state
    {
        get;
        private set;
    }
    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void OnEnable()
    {
        transform.SetParent(null);
    }

    private void Update()
    {
        switch (state)
        {
            case AIState.Idle:
                break;
            case AIState.MovingToNode:
                _OnUpdateMoveToNode();
                break;
            case AIState.InBuilding:
                break;
            case AIState.MovingToPosition:
                _OnUpdateMoveToPosition();
                break;
            case AIState.MovingToTrigger:
                break;
        }

    }

    private void OnDestroy()
    {
        
    }
    public void Init(EntityBank bank)
    {
        state = AIState.Idle;
        currentDesiredNodeType = NodeType.None;
        currNode = homeNode = GameManager.instance.GetClosestPhysicalNode(transform.position);
        nextNode = null;
        this.bank = bank;
    }

    internal void NotifyWithTask(TaskInstance instance)
    {
        MoveToTrigger(instance.TaskTrigger);
    }

    internal void GoHome()
    {
        MoveToNode(homeNode);
        nodeReachedAction = EnterHome;
    }

    private void EnterHome()
    {
        TransitionState(AIState.InBuilding);
    }

    private void ReachedNodeOfType(NodeType currentDesiredLocationType)
    {
        ReachedTargetNode();
        switch (currentDesiredLocationType)
        {
            case NodeType.None:
                break;
            case NodeType.Crossroads:
                break;
            case NodeType.Bunker:
                EnterBuilding(currNode.controller.GetComponent<BunkerController>());
                break;
            case NodeType.AlarmSystem:
                break;
            case NodeType.House:
                break;
        }
    }
    void ReachedTargetNode()
    {
        if (nodeReachedAction != null)
            nodeReachedAction();
    }
    public void EnterBuilding(IBuilding building)
    {
        occupiedBuilding = building;
        TransitionState(AIState.InBuilding);
    }
    public void MoveTo(Vector3 position)
    {
        currentTargetPosition = position;
        TransitionState(AIState.MovingToPosition);
        navMeshAgent.SetDestination(position);
    }

    public void MoveToTrigger(Collider targetTrigger)
    {
        if (!targetTrigger.isTrigger)
            throw new System.Exception("Tried to move entity to a trigger which was not marked as trigger!");

        currentTargetCollider = targetTrigger;
        TransitionState(AIState.MovingToTrigger);
        navMeshAgent.SetDestination(targetTrigger.transform.position);
    }        

    private void SetNextTargetNodeForDesiredLocationType()
    {
        nextNode = currNode.GetNextNodeForNodeType(currentDesiredNodeType);
        if (nextNode == null)
            TransitionState(AIState.Idle);
        else
            navMeshAgent.SetDestination(nextNode.controller.transform.position);
    }
    private void SetNextTargetNodeAlongCurrentPath()
    {
        if (currentPathIndex < currPathToDestinationNode.Count - 1)
        {
            currentPathIndex++;
            nextNode = currPathToDestinationNode[currentPathIndex].worldNode;
            navMeshAgent.SetDestination(nextNode.controller.transform.position);
        }
        else
        {
            TransitionState(AIState.Idle);
        }
    }

    public void BombAlarm()
    {
        MoveToNodeOfType(NodeType.Bunker);
    }

    private void MoveToNode(GraphNode targetNode)
    {
        destinationNode = targetNode;
        currNode = GameManager.instance.GetClosestPhysicalNode(transform.position);
        currentPathIndex = 0;
        currentDesiredNodeType = NodeType.None;

        if (GameManager.instance.GetPath(currNode, destinationNode, out currPathToDestinationNode))
        {
            nextNode = currPathToDestinationNode[1].worldNode;
        }
        else
        {
            nextNode = targetNode;
        }
        TransitionState(AIState.MovingToNode);
        navMeshAgent.SetDestination(nextNode.controller.transform.position);
    }
    private void MoveToNodeOfType(NodeType nodeType)
    {
        nextNode = GameManager.instance.GetClosestPhysicalNode(transform.position).GetNextNodeForNodeType(NodeType.Bunker);
        destinationNode = GameManager.instance.GetClosestNodeOfNodeType(nodeType, transform.position);
        TransitionState(AIState.MovingToNode);
        navMeshAgent.SetDestination(nextNode.controller.transform.position);
        currentDesiredNodeType = nodeType;
    }

    #region State Machine
    private void OnEnter(AIState targetState)
    {
        switch (targetState)
        {
            case AIState.Idle:
                break;
            case AIState.MovingToTrigger:
            case AIState.MovingToNode:
            case AIState.MovingToPosition:
                _OnEnterMoving();
                break;
            case AIState.InBuilding:
                _OnEnterBuilding();
                break;
        }
    } 
    private void OnExit(AIState state)
    {
        switch (state)
        {
            case AIState.Idle:
                break;
            case AIState.MovingToTrigger:
            case AIState.MovingToPosition:
            case AIState.MovingToNode:
                _OnExitMoving();
                break;
            case AIState.InBuilding:
                _OnExitBuilding();
                break;
        }
    }
    public void TransitionState(AIState targetState)
    {
        OnExit(state);
        OnEnter(targetState);
        state = targetState;
    }
    #endregion


    #region State Machine Callbacks
    private void _OnEnterMoving()
    {
        navMeshAgent.isStopped = false;
    }
    private void _OnExitMoving()
    {
        navMeshAgent.isStopped = true;
    }
    private void _OnEnterBuilding()
    {
        gameObject.SetActive(false);
        transform.SetParent(bank.transform);
    }
    private void _OnExitBuilding()
    {
        gameObject.SetActive(true);
        if (occupiedBuilding != null)
            transform.position = occupiedBuilding.ExitPosition;
        occupiedBuilding = null;
    }
    private void _OnUpdateMoveToPosition()
    {
        if (Vector3.Distance(currentTargetPosition, transform.position) < minPositionDist)
        {
            TransitionState(AIState.Idle);
            MessageHub.PushQueue(new Message(
                MessageType.EntityReachedPosition, 
                this, 
                currentTargetPosition));
        }
    }
    private void _OnUpdateMoveToNode()
    {
        if (nextNode == null)
        {
            TransitionState(AIState.Idle);
            return;
        }
        if (Vector3.Distance(nextNode.controller.transform.position, transform.position) < minNodeDist)
        {
            currNode = nextNode;
            if (currNode != destinationNode && 
                currentDesiredNodeType != NodeType.None)
            {
                SetNextTargetNodeForDesiredLocationType();
            }
            else if (currNode != destinationNode && 
                     currentDesiredNodeType == NodeType.None)
            {
                SetNextTargetNodeAlongCurrentPath();
            }
            else if (currNode == destinationNode &&
                     currentDesiredNodeType != NodeType.None)
            {
                ReachedNodeOfType(currentDesiredNodeType);
            }
            else if (currNode == destinationNode &&
                     currentDesiredNodeType == NodeType.None)
            {
                ReachedTargetNode();
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (state == AIState.MovingToTrigger &&
            other == currentTargetCollider)
        {
            TransitionState(AIState.Idle);
            MessageHub.PushQueue(new Message(
                MessageType.EntityReachedCollider,
                this,
                other));
        }
    }
    #endregion

}