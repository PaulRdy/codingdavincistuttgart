﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{
    [SerializeField] GameObject rubble;

    private void Awake()
    {
        GetComponentInChildren<MeshRenderer>().enabled = false;
        MessageHub.StartListening(MessageType.BombingExecuted, OnBombingExecuted);
    }

    private void OnBombingExecuted(Message obj)
    {
        Instantiate(rubble, this.transform.position, Quaternion.identity);
        Destroy(this.gameObject);
        MessageHub.StopListening(MessageType.BombingExecuted, OnBombingExecuted);
    }

}
