﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneSquadMove : MonoBehaviour
{
    [SerializeField] float moveSpeed = 20.0f;
    Vector3 dir;

    
    void Update()
    {
        transform.position += Time.deltaTime * dir * moveSpeed;
        if (this.transform.position.z < -20.0f)
            Destroy(this.gameObject);
    }

    public void Init (Vector3 dir)
    {
        this.dir = dir;
    }
}
