﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public enum NodeType
{
    None = -1,
    Crossroads,
    Bunker,
    AlarmSystem,
    House
}

public class GraphNode
{
    public List<GraphNode> connectedNodes;
    public event Action<GraphNode> OnDestroy;
    public event Action<NodeType, GraphNode> NextNodeForDestinationTypeUpdated;
    Dictionary<NodeType, GraphNode> nextNodeInDirectionOfNodeTypes;
    public NodeType type
    {
        get;
        private set;
    }
    public NodeController controller
    {
        get;
        private set;
    }
    public GraphNode(NodeType type)
    {
        connectedNodes = new List<GraphNode>();
        nextNodeInDirectionOfNodeTypes = new Dictionary<NodeType, GraphNode>();
        this.type = type;
        GameManager.instance.RegisterNode(type, this);
    }

    public void DeleteNode()
    {
        foreach(GraphNode n in connectedNodes)
        {
            n.connectedNodes.Remove(this);
        }
        OnDestroy(this);
        GameManager.instance.DeleteNode(this);
    }
    public void SetController(NodeController controller)
    {
        this.controller = controller;
    }

    public GraphEdge ConnectTo(GraphNode other)
    {
        this.connectedNodes.Add(other);
        return new GraphEdge(this, other);
    }
    public bool PathToClosestNodeType(NodeType type, out List<AstarNode> path)
    {
        List<GraphNode> nodes = GameManager.instance.graph.typeToNodes[type];
        List<Tuple<float, List<AstarNode>>> paths = new List<Tuple<float, List<AstarNode>>>();
        path = null;
        for (int i = 0; i < nodes.Count; i++)
        {
            List<AstarNode> tmp = new List<AstarNode>();
            if (GameManager.instance.GetPath(this, nodes[i], out tmp))
            {
                float pathCostTotal = tmp.Sum(n => n.gCost);
                paths.Add(new Tuple<float, List<AstarNode>>(pathCostTotal, tmp));
            }
        }

        if (paths.Count == 0)
            return false;
        paths = paths.OrderBy(p => p.Item1).ToList();
        path = paths[0].Item2;
        return true;
    }

    internal void ClearNextNodeDict()
    {
        nextNodeInDirectionOfNodeTypes.Clear();
    }

    public GraphNode GetNextNodeForNodeType(NodeType type)
    {
        if (this.type == type)
            return this;
        if (!nextNodeInDirectionOfNodeTypes.ContainsKey(type))
        {
            List<AstarNode> path;
            if (PathToClosestNodeType(type, out path))
            {
                SetNextNodeForNodeType(type, path[1].worldNode);
            }
            else
            {
                return null;
            }
        }
        return nextNodeInDirectionOfNodeTypes[type];
    }

    public void SetNextNodeForNodeType(NodeType type, GraphNode node)
    {
        if (!nextNodeInDirectionOfNodeTypes.ContainsKey(type))
        {
            nextNodeInDirectionOfNodeTypes.Add(type, node);
        }
        else
        {
            nextNodeInDirectionOfNodeTypes[type] = node;
        }
        if (NextNodeForDestinationTypeUpdated != null)
            NextNodeForDestinationTypeUpdated(type, node);
    }
    internal float EstimateHeuristic(GraphNode end)
    {
        return Vector3.Distance(controller.transform.position, 
                                end.controller.transform.position);
    }
}

