﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class GraphEdge
{
    GraphNode start;
    GraphNode end;

    public float weight = 1.0f;

    public GraphEdge (GraphNode start, GraphNode end)
    {
        this.start = start;
        this.end = end;
    }
}