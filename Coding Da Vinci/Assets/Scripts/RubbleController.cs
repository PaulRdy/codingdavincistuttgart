﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubbleController : MonoBehaviour
{
    [SerializeField] GameObject[] rubbleMeshes;
    [SerializeField] Transform meshParent;
    [SerializeField] Task removeTask;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(rubbleMeshes[UnityEngine.Random.Range(0, rubbleMeshes.Length - 1)], meshParent, false);
    }

    private void OnMouseDown()
    {
        if (GameManager.instance.state == GameState.Planning &&
            GameManager.instance.NumberOfCurrentActions > 0)
        {
            TaskManager.Instance.CreateTask(removeTask, transform.position, Removed);
            MessageHub.PushQueue(new Message(MessageType.GameplayActionComissioned, null));

        }
    }

    private void Removed(TaskInstance obj)
    {
        MessageHub.PushQueue(new Message(MessageType.GameplayActionExecuted, null));
        Destroy(this.gameObject);
    }
}
