﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum GameState
{
    Planning,
    Building,
    Tanking
}
public class GameManager : MonoBehaviour
{
    const int entityLoadingCoroutines = 16;
    public static GameManager instance
    {
        get
        {
            return _instance;
        }
    }
    static GameManager _instance;
    private void Awake()
    {
        Init();
        //Test();
        //TestPath();
    }
    private void Start()
    {
        nodeControllers.ForEach(nc => nc.Init());
    }
    [Header("Audio")]
    [SerializeField] AudioSource ambienceSource;
    [SerializeField] AudioSource alarmSource;
    [SerializeField] AudioSource flybySource;
    [SerializeField] AudioClip b25SFX;

    [Header("Misc")]
    [SerializeField] GameObject testnodePrefab;
    [SerializeField] GameObject entityPrefab;
    [SerializeField] GameObject bombMarkerPrefab;
    [SerializeField] EntityBank entityBank;
    [SerializeField] GameObject rubblePrefab;
    [SerializeField] GameObject previewCamera;
    [SerializeField] Task streetSignTask;
    [SerializeField] Camera secondaryCamera;
    [SerializeField] GameObject planeSquad;
    [SerializeField] BoxCollider mapBounds;
    [SerializeField] Transform planesSpawn, planesGoal;
    public float bombingWarningTime = 10.0f;
    public float bombingTime = 5.0f;
    public float afterBombingTime = 10.0f;
    public BidirectionalPointGraph graph;
    public Dictionary<NodeType, GraphNode> typeToNode;
    public List<NodeController> nodeControllers;
    public List<EntityController> entities;
    public MessageHub messageHub;
    public Transform spawnPos;

    public int NumberOfCurrentActions { get; private set; }
    public int MaxNumberOfActions { get { return maxNumberOfActions; } }
    [SerializeField] int maxNumberOfActions;

    Dictionary<TaskInstance, Tuple<NodeController, NodeController>> taskToNodes;
    private NodeConnectionManager nodeConnector;

    #region State Machine
    public GameState state
    {
        get { return _state; }
        private set
        {
            ExitState(_state);
            _state = value;
            EnterState(_state);
        }
    }

    private void ExitState(GameState state)
    {
        switch (state)
        {
            case GameState.Planning:
                selectedNode = null;
                secondaryCamera.enabled = false;
                break;
            case GameState.Building:
                break;
            case GameState.Tanking:
                break;
        }
    }

    private void EnterState(GameState state)
    {
        switch (state)
        {
            case GameState.Planning:
                secondaryCamera.enabled = true;
                break;
            case GameState.Building:
                break;
            case GameState.Tanking:
                break;
        }
    }

    private GameState _state;
    #endregion


    public NodeController selectedNode
    {
        get; private set;
    }
    private NodeController nodeToConnectSelectedNodeTo;


    private void Init()
    {
        _instance = this;
        nodeControllers = new List<NodeController>();
        graph = new BidirectionalPointGraph();
        messageHub = new MessageHub();
        entities = new List<EntityController>();
        nodeConnector = GetComponent<NodeConnectionManager>();
        UI.__INIT();
        StartCoroutine(LoadGameRoutine());
    }
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    GraphNode nodeToSpawnAt = GetRandomNode();
        //    SpawnEntityAtNode(nodeToSpawnAt);
        //}
        //if (Input.GetKeyDown(KeyCode.B))
        //{
        //    MessageHub.PushQueue(new Message(MessageType.BombAlarmSounded, null));
        //}

        while (messageHub._PopQueue())
        {

        }
    }

    IEnumerator LoadGameRoutine ()
    {
        SubscribeMessages();
        taskToNodes = new Dictionary<TaskInstance, Tuple<NodeController, NodeController>>();
        yield return new WaitForFixedUpdate();
        messageHub.BroadcastImmediate(new Message(MessageType.GameLoadingStarted, null));

        HouseController[] houseControllers = FindObjectsOfType<HouseController>();
        for (int i = 0; i < houseControllers.Length; i += entityLoadingCoroutines)
        {
            for(int j = 0; j < entityLoadingCoroutines - 1; j++)
            {
                UI.Instance.LoadingDetailText.text = "entities " + entities.Count.ToString();
                if (i+j< houseControllers.Length)
                {
                    HouseController hc = houseControllers[i + j];
                    hc.StartCoroutine(hc.SpawnEntitiesRoutine(entityBank));
                }
            }
            if (i + entityLoadingCoroutines - 1 < houseControllers.Length)
            {
                HouseController hc = houseControllers[i + entityLoadingCoroutines - 1];
                yield return hc.StartCoroutine(hc.SpawnEntitiesRoutine(entityBank));
            }
        }

        messageHub.BroadcastImmediate(new Message(MessageType.GameLoadingFinished, null));
        StartTurn();
    }

    void StartTurn()
    {
        NumberOfCurrentActions = maxNumberOfActions;
        state = GameState.Planning;
        MessageHub.PushQueue(new Message(MessageType.TurnStarted, null));
    }

    private void SubscribeMessages()
    {
        MessageHub.StartListening(MessageType.NodeSelected, OnNodeSelected);
        MessageHub.StartListening(MessageType.GameplayActionExecuted, OnActionExecuted);
        MessageHub.StartListening(MessageType.GameplayActionComissioned, OnActionComissioned);
    }

    private void OnActionComissioned(Message obj)
    {
        NumberOfCurrentActions--;
    }


    #region Street sign placement
    private void OnNodeSelected(Message obj)
    {
        NodeController node = (NodeController)obj.data[0];
        switch (state)
        {
            case GameState.Planning:         
                if (selectedNode == null)
                {
                    SelectNode(node);
                }
                else
                {
                    TryProposeConnectionTask(selectedNode, node);
                }
                break;
            case GameState.Building:
                break;
            case GameState.Tanking:
                break;
        }
    }

    private void TryProposeConnectionTask(NodeController selectedNode, NodeController node)
    {
        Ray ray = new Ray(selectedNode.transform.position, node.transform.position);
        if (selectedNode != node &&
            NumberOfCurrentActions > 0 &&
            !Physics.Raycast(
                ray, 
                Vector3.Distance(selectedNode.transform.position, node.transform.position), 
                ~LayerMask.GetMask("Nodes")))
        {
            nodeToConnectSelectedNodeTo = node;
            MessageHub.PushQueue(new Message(MessageType.NodeConnectionProposed, selectedNode, node));
            MessageHub.StartListening(MessageType.UINodeConnectionAccepted, ConnectionAccepted);
            MessageHub.StartListening(MessageType.UINodeConnectionDeclined, ConnectionDeclined);
        }
        else
        {
            ResetNodeSelection();
        }
    }

    private void ConnectionDeclined(Message obj)
    {
        ResetNodeSelection();
    }   

    private void ConnectionAccepted(Message obj)
    {
        if (selectedNode != null && nodeToConnectSelectedNodeTo != null)
        {
            TaskInstance task = TaskManager.Instance.CreateTask(streetSignTask, selectedNode.transform.position, OnStreetSignCreated);
            taskToNodes.Add(
                task,
                new Tuple<NodeController, NodeController>(
                    selectedNode,
                    nodeToConnectSelectedNodeTo
                ));

            MessageHub.PushQueue(new Message(MessageType.GameplayActionComissioned, null));
            ResetNodeSelection();
        }
    }
    private void ResetNodeSelection()
    {
        try
        {
            MessageHub.StopListening(MessageType.UINodeConnectionAccepted, ConnectionAccepted);
            MessageHub.StopListening(MessageType.UINodeConnectionDeclined, ConnectionDeclined);
        } catch (MessagingException e)
        {

        }
        selectedNode = null;
        nodeToConnectSelectedNodeTo = null;
    }

    private void SelectNode(NodeController node)
    {
        this.selectedNode = node;
        previewCamera.transform.position = selectedNode.transform.position;
    }

    private void OnStreetSignCreated(TaskInstance obj)
    {
        NodeController from = taskToNodes[obj].Item1;
        NodeController to = taskToNodes[obj].Item2;

        nodeConnector.ConnectNodes(from, to);
        from.node.SetNextNodeForNodeType(NodeType.Bunker, to.node);
        taskToNodes.Remove(obj);
        MessageHub.PushQueue(new Message(MessageType.GameplayActionExecuted, null));
    }
    #endregion


    #region Bombing handling
    private void OnActionExecuted(Message obj)
    {
        if (NumberOfCurrentActions <= 0)
        {
            StartBombing();
            StartCoroutine(BombingRoutine());
        }
    }

    private void StartBombing()
    {
        state = GameState.Tanking;
        MessageHub.PushQueue(new Message(MessageType.BombAlarmSounded, null));
        entities.ForEach(e => e.BombAlarm());
        alarmSource.Play();
    }
    IEnumerator BombingRoutine()
    {
        yield return new WaitForSeconds(bombingWarningTime);
        MessageHub.PushQueue(new Message(MessageType.BombingStarted, null));
        SpawnPlanes();
        flybySource.PlayOneShot(b25SFX);

        yield return new WaitForSeconds(bombingTime);
        MessageHub.PushQueue(new Message(MessageType.BombingExecuted, null));
        CalculateBombDamage();
        yield return new WaitForSeconds(afterBombingTime);
        MessageHub.PushQueue(new Message(MessageType.BombingOver, null));
        entities.ForEach(e => e.GoHome());
        alarmSource.Stop();
        yield return null;
        StartTurn();
    }

    private void SpawnPlanes()
    {
        PlaneSquadMove psm = Instantiate(planeSquad, planesSpawn.position, Quaternion.identity).GetComponent<PlaneSquadMove>();
        Vector3 dir = (planesGoal.position - planesSpawn.position).normalized;
        psm.Init(dir);
        psm.transform.LookAt(psm.transform.position - dir);
    }

    private void CalculateBombDamage()
    {

    }
    #endregion


    internal GraphNode GetClosestPhysicalNode(Vector3 position)
    {
        List<NodeController> sorted = nodeControllers.OrderBy(n => Vector3.Distance(n.transform.position, position)).ToList();
        return sorted[0].node;
    }
    internal GraphNode GetClosestNodeOfNodeType(NodeType type, Vector3 position)
    {
        List<NodeController> nodesOfType = nodeControllers.Where(nc => nc.node.type == type).ToList();
        nodesOfType = nodesOfType.OrderBy(nc => Vector3.Distance(nc.transform.position, position)).ToList();
        return nodesOfType[0].node;
    }
    internal void RegisterNode(NodeType type, GraphNode graphNode)
    {
        graph.AddNode(type, graphNode);
    }

    internal void DeleteNode(GraphNode graphNode)
    {
        throw new NotImplementedException();
    }
    
    private GraphNode GetRandomNode()
    {
        GraphNode[] nodes = graph.nodes.ToArray();
        GraphNode nodeToSpawnAt = nodes[UnityEngine.Random.Range(0, nodes.Length)];
        return nodeToSpawnAt;
    }

    private EntityController SpawnEntityAtNode(GraphNode nodeToSpawnAt)
    {
        Vector3 spawnPos = nodeToSpawnAt.controller.transform.position;
        return SpawnEntity(spawnPos);
    }
        

    public EntityController SpawnEntity(NodeController controller)
    {
        return SpawnEntityAtNode(controller.node);
    }
    public EntityController SpawnEntity(Vector3 position)
    {
        GameObject inst = Instantiate(entityPrefab, position, Quaternion.identity);
        EntityController instEntity = inst.GetComponent<EntityController>();
        entities.Add(instEntity);
        return instEntity;
    }

    public BombController SpawnBomb(Vector3 position)
    {
        return GameObject.Instantiate(bombMarkerPrefab, position, Quaternion.identity).GetComponent<BombController>();
    }
    
    public RubbleController SpawnRubble(Vector3 position)
    {
        return GameObject.Instantiate(rubblePrefab, position, Quaternion.identity).GetComponent<RubbleController>();
    }

    public bool GetPath(GraphNode start, GraphNode end, out List<AstarNode> output)
    {
        if (start == null || end == null)
            throw new System.NullReferenceException("Tried to get path but start or end node was null");
        output = new List<AstarNode>();
        if (start == end)
        {
            return false;
        }
        List<AstarNode> open = new List<AstarNode>();
        List<AstarNode> close = new List<AstarNode>();

        open.Add(new AstarNode(0f, 
                               start.EstimateHeuristic(end), 
                               start, 
                               null));
        AstarNode currNode = open[0];
        int safety = 0;
        bool foundPath = false;
        while (open.Count > 0)
        {
            currNode = open[0];
            foreach (GraphNode neighbourInWorld in currNode.worldNode.connectedNodes)
            {
                GraphEdge edge = graph.GetEdge(currNode.worldNode, neighbourInWorld);
                if (edge == null)
                    throw new System.Exception("tried to get invalid edge");

                AstarNode closeCheck = close.FirstOrDefault(n => n.worldNode == neighbourInWorld);
                if (closeCheck != default(AstarNode))
                {
                    continue;
                }

                AstarNode openCheck = open.FirstOrDefault(n => n.worldNode == neighbourInWorld);
                if (openCheck == default(AstarNode))
                {
                    open.Add(new AstarNode(currNode.gCost + edge.weight, 
                                           neighbourInWorld.EstimateHeuristic(end), 
                                           neighbourInWorld, 
                                           currNode));
                }
                else
                {
                    openCheck.gCost = currNode.gCost + edge.weight;
                }                
            }
            close.Add(currNode);

            if (currNode.worldNode == end)
            {
                foundPath = true;
                break;
            }
            open.Remove(currNode);
            open = open.OrderBy(n => n.fCost).ToList();

            if (safety > 100)
            {
                throw new Exception("astar went out of bounds");
            }
            safety++;
        }
        if (currNode.prevNode == null ||
            !foundPath)
        {
            return false;
        }
        AstarNode retrace = currNode;
        while (true)
        {
            output.Add(retrace);
            if (retrace.prevNode == null)
                break;
            retrace = retrace.prevNode;
        }
        output.Reverse();
        return true;
    }


}
