﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseController : MonoBehaviour, IBuilding
{
    [SerializeField] Transform entitySpawnPos;
    [SerializeField] int defaultNumberOfInhabitants = 4;
    [SerializeField] float minSpawnInterval = 0.2f;
    [SerializeField] float maxSpawnInterval = 0.4f;
    int numberOfInhabitants;

    Coroutine leaveHouseRoutine;

    public Vector3 ExitPosition => entitySpawnPos.position;

    private void Awake()
    {
        numberOfInhabitants = defaultNumberOfInhabitants;
        //MessageHub.StartListening(MessageType.BombAlarmSounded, OnAlarmSounded, MethodPriority.Filter);


    }

    private void OnAlarmSounded(Message obj)
    {
        //if (leaveHouseRoutine == null)
        //    leaveHouseRoutine = StartCoroutine(LeaveHouseRoutine());
    }

    public IEnumerator SpawnEntitiesRoutine(EntityBank bank)
    {
        for (int i = 0; i < defaultNumberOfInhabitants; i++)
        {
            EntityController e = GameManager.instance.SpawnEntity(entitySpawnPos.position);
            e.Init(bank);
            e.EnterBuilding(this);
            yield return null;
        }
    }

    //IEnumerator LeaveHouseRoutine()
    //{
    //    while (numberOfInhabitants > 0)
    //    {
    //        yield return new WaitForSeconds(UnityEngine.Random.Range(minSpawnInterval, maxSpawnInterval));
    //        int max = UnityEngine.Random.Range(1, numberOfInhabitants + 1);
    //        for (int i = 0; i < max; i++)
    //        {
    //            GameManager.instance.SpawnEntity(entitySpawnPos.position).Alert();
    //        }
    //        numberOfInhabitants -= max;
    //    }
    //    leaveHouseRoutine = null;
    //}
}
