﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class NodeController : MonoBehaviour
{
    [SerializeField]
    public List<NodeController> defaultConnectedNodes;
    public GraphNode node;
    public int numberOfClosestNodesOnStart = 3;
    public NodeType type;
    public GameObject directionIndicator;
    private void Awake()
    {
        GameManager.instance.nodeControllers.Add(this);
        MessageHub.StartListening(MessageType.NodeConnectionChanged, OnNodeConnectionChanged);
        if (node == null)
        {
            node = new GraphNode(type);
            node.SetController(this);
        }
        node.NextNodeForDestinationTypeUpdated += Node_NextNodeForDestinationTypeUpdated;

    }
    public void SetGraphNode(GraphNode node)
    {
        this.node = node;
    }
    public void Init()
    {        
        if (type != NodeType.Crossroads)
        {
            directionIndicator.SetActive(false);
        }
        node.GetNextNodeForNodeType(NodeType.Bunker);
    }

    private void Node_NextNodeForDestinationTypeUpdated(NodeType arg1, GraphNode arg2)
    {
        if (arg1 == NodeType.Bunker)
        {
            directionIndicator.transform.LookAt(arg2.controller.directionIndicator.transform);
        }
    }

    private void Start()
    {
        foreach(NodeController nc in defaultConnectedNodes)
        {
            try
            {
                GameManager.instance.graph.AddEdge(this.node, nc.node);
            }
            catch (GraphException e)
            {
                Debug.LogError(e);
            }
        }

        List<NodeController> ordered = GameManager.instance.nodeControllers.OrderBy(                                                
            c => Vector3.Distance(c.transform.position, this.transform.position)).ToList();
        for (int i = 0; i < numberOfClosestNodesOnStart; i++)
        {
            GameManager.instance.graph.AddEdge(node, ordered[i].node);
        }
    }
    

    private void OnMouseDown()
    {
        MessageHub.PushQueue(new Message(MessageType.NodeSelected, this));
    }

    private void OnNodeConnectionChanged(Message obj)
    {
        node.ClearNextNodeDict();
    }
}
