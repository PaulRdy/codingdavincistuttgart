﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeConnectionManager : MonoBehaviour
{
    [SerializeField] bool debugMode = false;
    NodeController lastSelectedController;

    private void Awake()
    {
        if (debugMode)
                MessageHub.StartListening(MessageType.NodeSelected, OnNodeSelected);
    }

    private void OnNodeSelected(Message obj)
    {
        NodeController selectedController = (NodeController)obj.data[0];       
        if (lastSelectedController != null)
        {
            if (selectedController != lastSelectedController)
            {
                ConnectNodes(lastSelectedController, selectedController);
                lastSelectedController.node.SetNextNodeForNodeType(NodeType.Bunker, selectedController.node);
            }
            lastSelectedController = null;
        }
        else
        {
            lastSelectedController = selectedController;
        }
    }

    public void ConnectNodes(NodeController from, NodeController to)
    {
        if (!from.node.connectedNodes.Contains(to.node))
        {
            GameManager.instance.graph.AddEdge(from.node, to.node);
            Debug.Log("connected: " + from.name + " & " + to.name); 
            MessageHub.PushQueue(new Message(MessageType.NodeConnectionChanged, null));
        }
        else
            Debug.LogWarning("Trying to connect nodes  which are already connected");
    }
}
