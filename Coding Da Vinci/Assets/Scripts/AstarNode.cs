﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class AstarNode
{
    public float gCost;
    public float hCost;
    public float fCost
    {
        get { return gCost + hCost; }
    }

    public GraphNode worldNode;
    public AstarNode prevNode;

    public AstarNode(float gCost, float hCost, GraphNode worldNode, AstarNode prevNode)
    {
        this.gCost = gCost;
        this.hCost = hCost;
        this.worldNode = worldNode;
        this.prevNode = prevNode;
    }
}

