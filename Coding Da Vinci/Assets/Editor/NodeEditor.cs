﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
[CustomEditor(typeof(NodeController))]
public class NodeEditor : Editor
{
    NodeController ctrl;
    private void OnEnable()
    {
        ctrl = (NodeController)target;
        
    }

    private void OnSceneGUI()
    {
        if (ctrl.defaultConnectedNodes != null &&
            ctrl.defaultConnectedNodes.Count > 0)
        {
            foreach (NodeController other in ctrl.defaultConnectedNodes)
            {
                if (other == null) continue;
                Handles.color = Color.magenta;
                Handles.DrawAAPolyLine(10.0f, ctrl.transform.position, other.transform.position);
            }
        }
        Event e = Event.current;
        if (e != null)
        {
            switch (e.type)
            {
                case EventType.KeyDown:
                    if (e.keyCode == KeyCode.G)
                    {
                        ctrl = (NodeController)target;
                        TryAddNode(e);
                    }
                    break;
            }
            //DebugRay(e);
            
        }
        

    }

    private void DebugRay(Event e)
    {
        Ray ray = Camera.current.ScreenPointToRay(e.mousePosition);

        Debug.DrawRay(ray.origin, ray.direction, Color.blue, 10);
    }

    private void TryAddNode(Event e)
    {
        Ray ray = HandleUtility.GUIPointToWorldRay(e.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Nodes")))
        {
            NodeController hitNode = hit.collider.gameObject.GetComponent<NodeController>();
            if (hitNode != null)
            {
                
                if (ctrl.defaultConnectedNodes.Contains(hitNode))
                {
                    Undo.RegisterCompleteObjectUndo(ctrl, "Remove node connection");
                    EditorUtility.SetDirty(ctrl);
                    ctrl.defaultConnectedNodes.Remove(hitNode);
                }
                else
                {
                    Undo.RegisterCompleteObjectUndo(ctrl, "Add node connection");
                    EditorUtility.SetDirty(ctrl);
                    ctrl.defaultConnectedNodes.Add(hitNode);
                }
                EditorApplication.delayCall += () =>
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(ctrl);
                };
            }
        }


    }
}
